								-------
						ConfigManager description
								-------
							Robert Schuster
								-------
							Date 2006-12-11

A brief Configuration Manager description

* Intro

	With the advent of tarent-contact becoming an applicatin platform it became
	highly necessary to design a future-proof way of obtaining and processing its
	configuration.

 	The missing proper configuration mechanism in tarent-contact resulted in a lot
	of code. Eg. classes where locating and parsing the XML documents although this
	could have generalized.

* Terms and Definitions

	[document] In order to abstract from the way the configuration is obtained
	we speak of configuration documents. Although those documents are usually
	stored in local files they could also be obtained from a secure webserver
	a WebDAV repository or else.

	[scope] The configuration system knows three scopes which allow the site 
	administrator to customize the software to the user's meets. The application
	defines site, installation and user scope. A later configuration scope can
	override and enhance a former scope but not all configuration kinds allow
	this.

	[kind] The configuration kind is another way of separating the configuration
	data. This time it is split by the meaning for the application. The three
	configuration kinds are the appearance, environment and the preferences kind.

	[variant] The tarent-contact code is not a single application anymore instead
	an application is built by creating a custom configuration. In order to allow
	multiple tarent-contact-based applications be part of a single installation
	the variant was introduced. It simply separates the configuration documents
	of different applications from each other.

* Specification

 	The following table shows the different configuration
	kinds, scopes and whether valid configuration data for
	a certain combintation is required, optional or not allowed.

*------------*------------*-------------*-------------*
|           | Appearance | Environment | Preferences |
*-----------*------------*-------------*-------------*
| Site scope| mandatory  | optional    |   n/a       |
*-----------*------------*-------------*-------------*
| Installation | optional | optional   |  n/a        |
| scope        |          |            |             |
*--------------*----------*------------*-------------*
| User scope   | n/a      | optional   | optional    |
*--------------*----------*------------*-------------*

	Note: The current implementation is less strict on the fact
	that a site appearance configuration is missing. It will continue
	if an installation appearance configuration can be found.

* General

	Each configuration scope comprises of at least 2 documents, namely
	appearance.xml and environment.xml. Additionally there is usually
	a document defining actions and another one that defines plugins.

* Appearance

	The appearance configuration controls what kind of application the
	tarent-contact code is. Depending on the enabled set of GUI components
	you may either get a contact managements software or an email distribution
	system.

** document structure
 
	The root node of the appearance.xml must be 'appearance' in which
	the following tags are allowed:

*** include
 
	Via the 'file' attribute another document can be specified which contains
	another appearance configuration. The other document is evaluated completely
	before parsing continues.

*** plugins
 
	Via the 'file' attribute a document can be specified which contains
	the plugin definitions. The other document is evaluated completely before
	parsing continues.

*** actions
 
	Via the 'file' attribute a document can be specified which contains
	the action definitions. The other document is evaluated completely before
	parsing continues.

*** param
 
	Via the 'name' attribute a name can for a certain parameter can be specified.
	Those names *MUST* exist as instances in the class Appearance.Key otherwise
	parsing will detect this error. The value of the parameter is to be set
	via an attribute named 'value'.

* Environment 

	The environment configuration contains information on how the application
	interacts with its environment. Eg. what server it talks to, which database
	is used and what kind of office application generates the documents.

** document structure
 
	All information is to be found in a document named environment.xml. Its 
	structure is the same as for the appearance.xml except that there can be
	no 'plugins' and 'actions' tags. Included documents are supposed to be
	environment configurations, too and parameter names must exist as instances
	of the class Environment.Key .

* Bootstrap configuration

	As another part of the configuration system is the data that controls how
	the configuration documents are to be obtained. This is called the bootstrap
	configuration and is configured via Java's system properties. However in order
	to make things as easy as possible tarent-contact falls back to sane default
	values if the bootstrap configuration does not exist.

** configuration type
 
	The system property "de.tarent.contact.config.bootstrap.type" controls how
	the configuration documents are obtained. At this time only "file" is defined
	which means that the documents are read from the local filesystem. Details on
	that are in another section. If the property is not set "file" is taken as
	default.

** application variant
 
	The system property "de.tarent.contact.config.bootstrap.variant" controls
	the application variant. At this time only "contact" is known which describes
	an email distribution system.

** Starter class
 
	Designed but not implemented yet is a way of getting the applications starter
	class via a system property. If set it can provide a unique way of invoking a
	tarent-contact based application. *This feature is not implemented,yet.*

** specifics of the file based loader
 
	The document loader which reads from the local filesystem can be configured
	via system properties but provides enough logic to find the configuration files
	even if no property is set.

	For the exact system property names refer to the documentation of the
	de.tarent.config.FileLoader class.

*** global vs. local vs. posix installation
 
	A global installation means that on a single machine multiple user use the
	same set of installed files to run tarent-contact-based application. This is
	the case when the installation goes to /opt on Unices or c:/Programs/tarent
	(or similar) on Windows machines.

	A local installation has taken place when a user installs into his or her home
	directory.
 
	Finally a posixy installation means that on a Unix machine the applications
	configuration files go to /etc and the code to /usr/share/java and so on.

	For a global and a posixy installation the file loader needs properly set
	system properties. In case of a local install it can guess the configuration
	file locations.

	If the file loader's class is contained in a file (= jar) it guesses a
	genuine local installation. If the class lies within a directory (not zipped)
	it guesses that this must be a development build.
  
* Outdated configuration

	In the directory src/main/old_configurations the older config.xml-like configuration
	files have been moved. When there is a need for one they can be migrated to the new
	format easily. The entries just need to be assigned to either the appearance or
	the environment configuration.