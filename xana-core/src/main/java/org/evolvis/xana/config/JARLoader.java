/**
 * 
 */
package org.evolvis.xana.config;

import java.io.InputStream;

import org.evolvis.xana.config.ConfigManager.DocumentUnavailableException;
import org.evolvis.xana.config.ConfigManager.Loader;
import org.evolvis.xana.config.ConfigManager.Scope;
import org.w3c.dom.Document;


/**
 * 
 * A loader capable of loading site and installation scope documents from the
 * classpath using a class loader.
 * 
 * <p>User scope configuration documents are read from the file system.</p>
 * 
 * <p>The base path for the documents <em>must</em>  be configured using a system property
 * which is created by the provided base argument and the <code>.jar.path</code>
 * suffix.</p>
 * 
 * <p>In case the base argument is <code>null</code> the base defaults to
 * <code>de.tarent.commons.config</code> for compatibility reasons.</p>
 * 
 * <p>In case the evaluated property is <code>null</code> all operations
 * are delegated to a file loader.</p>
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 * @author Robert Schuster (r.schuster@tarent.de) tarent GmbH Bonn
 *
 */
public class JARLoader extends Loader {
	
    String applicationId;
  
	Class applicationClass;
    
    final String PATH = BASE + ".path";
    
    String basePath;
    
    /**
     * <code>FileLoader</code> instance to handle all the user-scope
     * actions.
     */
    FileLoader fileLoader;
	
	JARLoader(String base, String variant, String applicationId, Class applicationClass) {
		super(base, variant);

        basePath = System.getProperty(PATH);

        this.applicationId = applicationId;
		
		this.applicationClass = applicationClass;
        
        fileLoader = new FileLoader(base, variant, applicationId, applicationClass);
	}

	/**
	 * @see ConfigManager.Loader#getDocument(ConfigManager.Scope, java.lang.String)
	 */
	protected Document getDocument(Scope scope, String docName)
			throws DocumentUnavailableException {
        if (basePath == null)
          return fileLoader.getDocument(scope, docName);

        // Delegate to the FileLoader.
		if(scope == Scope.USER)
			return fileLoader.getDocument(Scope.USER, docName);
		
		InputStream is = applicationClass.getResourceAsStream(basePath+scope.toString().toLowerCase()+"/"+docName);
		
		if(is == null)
			throw new RuntimeException("InputStream is null, cannot resolve \""+basePath+scope.toString().toLowerCase()+"/"+docName+"\"");
		
		try	{
			// FIXME The method #touri isn't java 1.4 compatible and will end with an runtime error!
			return XmlUtil.getParsedDocument(is, applicationClass.getResource(basePath+scope.toString().toLowerCase()+"/").toString());
		}
		catch (XmlUtil.Exception xmlue)	{
			throw new ConfigManager.DocumentUnavailableException("", "xml parsing error in \""+basePath+scope.toString().toLowerCase()+"/"+docName+"\"");
//		} catch (URISyntaxException e) {
//			throw new ConfigManager.DocumentUnavailableException("", "xml parsing error in \""+basePath+scope.toString().toLowerCase()+"/"+docName+"\"");
		}
	}
    
    protected void storeDocument(Scope scope, String docName, Document doc)
    throws ConfigManager.DocumentUnavailableException
    {
      // Just delegate.
      if (basePath == null || scope == Scope.USER)
       fileLoader.storeDocument(Scope.USER, docName, doc);
      
      throw new RuntimeException();
    }

	/**
	 * @see ConfigManager.Loader#isStoringSupported()
	 */
	protected boolean isStoringSupported(Scope scope) {
      if (basePath == null)
        return fileLoader.isStoringSupported(scope);
      
      // Store support is only available for user scope.
      return scope == Scope.USER;
	}
}
