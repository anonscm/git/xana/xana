package org.evolvis.xana.utils;

public class IconFactoryException extends Exception {

    private static final long serialVersionUID = -5951594506802243978L;

    public IconFactoryException() {
        super();
    }

    public IconFactoryException( String message ) {
        super( message );
    }

    public IconFactoryException( String message, Throwable cause ) {
        super( message, cause );
    }

    public IconFactoryException( Throwable cause ) {
        super( cause );
    }

}
