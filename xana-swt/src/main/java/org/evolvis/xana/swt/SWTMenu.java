/**
 * 
 */
package org.evolvis.xana.swt;

import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javax.swing.Action;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Decorations;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.evolvis.xana.action.ActionContainer;
import org.evolvis.xana.action.ActionRegistry;
import org.evolvis.xana.action.MenuHelper;
import org.evolvis.xana.action.TarentGUIAction;
import org.evolvis.xana.swt.utils.SWTActionContainerHelper;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SWTMenu implements ActionContainer {

	protected String uniqueName;
	protected ResourceBundle resourceBundle;
	protected Menu menu;
	protected Decorations parent;
	protected static final String DEFAULT_MENU = "Extras";
	protected final static Logger logger = Logger.getLogger(SWTMenu.class.getName());
	protected Map<String, Menu> menus = new HashMap<String, Menu>();
	
	public SWTMenu( String uniqueName, ResourceBundle resourceBundle, Decorations parent) {
		this(uniqueName, resourceBundle, parent, SWT.BAR);
	}
	
	public SWTMenu( String uniqueName, ResourceBundle resourceBundle, Decorations parent, int style) {
		menu = new Menu(parent, style);
        this.uniqueName = uniqueName;
        this.resourceBundle = resourceBundle;
        this.parent = parent;
    }
	
	/**
	 * @see org.evolvis.xana.action.ActionContainer#attachGUIAction(javax.swing.Action, java.lang.String)
	 */
	public void attachGUIAction(final Action action, String menuPath) {
		
		Menu submenu = getSubMenu(menuPath);
		
		MenuItem item = new MenuItem (submenu, SWT.PUSH);
		item.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				SWTMenu.this.getMenu().getDisplay().asyncExec(new Runnable() {
					
					public void run() {
						action.actionPerformed(new ActionEvent(menu, 0, ""));
					}
				});
			}
		});
		
		Image icon = (Image)action.getValue(Action.SMALL_ICON);
		
		if(icon != null)
			item.setImage(icon);
		
		item.setText(action.getValue(Action.NAME).toString());
		
		// Update enabled-state of ToolItem if enabled-state of action changed 
		SWTActionContainerHelper.addEnabledListener(item, action);
	}

	/**
	 * @see org.evolvis.xana.action.ActionContainer#getContainerUniqueName()
	 */
	public String getContainerUniqueName() {
		return uniqueName;
	}

	/**
	 * @see org.evolvis.xana.action.ActionContainer#initActions()
	 */
	public void initActions() {
		List<TarentGUIAction> actions = ActionRegistry.getInstance().getActions(getContainerUniqueName());
		
		Collections.sort(actions, new Comparator<TarentGUIAction>() {

			/**
			 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
			 */
			public int compare(TarentGUIAction action1, TarentGUIAction action2) {
				Integer priority1 = MenuHelper.getAssignedMainMenuPriority(action1.getMenuPath(getContainerUniqueName()));
				Integer priority2 = MenuHelper.getAssignedMainMenuPriority(action2.getMenuPath(getContainerUniqueName()));
				logger.finer("comparing main-menu-priorities "+priority1+" and "+priority2);
				return priority1.compareTo(priority2);
			}
			
		});
		
		Iterator<TarentGUIAction> actionsIt = actions.iterator();
		
		while(actionsIt.hasNext()) {
			TarentGUIAction action = actionsIt.next();
			attachGUIAction(action, action.getMenuPath(getContainerUniqueName()));
		}
	}

	/**
	 * @see org.evolvis.xana.action.ActionContainer#removeGUIAction(javax.swing.Action)
	 */
	public void removeGUIAction(Action arg0) {
		// TODO Auto-generated method stub

	}
	
	public Menu getMenu() {
		return menu;
	}
	
    protected String[] getMenuSteps( String menuPath ) {
        String[] menuSteps;
        if ( null == menuPath || "".equals( menuPath ) ) {
            menuSteps = new String[] { DEFAULT_MENU };
        }
        else {
            String[] menuPathParts = menuPath.split( ":" );
            menuSteps = menuPathParts[0].split( "/" );
        }
        return menuSteps;
    }
    
    protected String removeMainMenuPriority(String menuPath)
    {
    	String[] menuPathParts = null;
    	if(menuPath != null)
    		menuPathParts = menuPath.split(":");
    	else
    		return null;
    	
    	// check if the menuPath starts with a digit
    	if(menuPathParts != null && menuPathParts.length > 0 && Character.isDigit(menuPathParts[0].charAt(0))) {
    		// this menuPath contains a priority-value for the main-menu. remove it
    		return menuPath.substring(menuPath.indexOf(':')+1);
    	}
    	
    	// does not contain a priority-value for the main-menu. return without change
    	return menuPath;
    }
    
    protected Menu getSubMenu(String menuPath) {   	
    	String[] menuSteps = getMenuSteps(removeMainMenuPriority(menuPath));
    	
    	Menu submenu = menus.get(menuSteps[0]);
    	
    	if(submenu == null) {
    		MenuItem menuBarItem = new MenuItem (menu, SWT.CASCADE);
    		menuBarItem.setText (menuSteps[0]);
    		submenu = new Menu (parent, SWT.DROP_DOWN);
    		menuBarItem.setMenu (submenu);
    		menus.put(menuSteps[0], submenu);
    	}
    	
    	return submenu;
    }
}