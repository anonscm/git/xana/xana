package org.evolvis.xana.config;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import org.evolvis.xana.config.ConfigManager.Scope;
import org.w3c.dom.Node;

/**
 * Provides base functionality for configuration item
 * retrieval and value conversion.
 * 
 * <p>This class is very similar to the {@link de.tarent.config.Appearance}
 * class. Please refer to it for detailed information.</p>
 * 
 * <p><code>Environment</code> contains only parameter configuration
 * and you can access them via a fixed set of {@link Environment.Key}
 * instances.</p>
 * 
 * <p>For a general introduction to the configuration system
 * refer to the respective document on the project's website.</p>
 */
public final class Environment extends Base
{
  private LinkedHashMap connectionDefinitions = new LinkedHashMap();
  
  private String defaultConnection;
  
  private boolean userOverridable = true;
  
  Environment()
  {
  }
  
  /**
   * Returns a map containing all the documents
   * which should be written in order to make the
   * environment's state persistant.
   * 
   * <p>The map's keys are the document names while
   * the values are the XML documents itself.</p>
   * 
   * <p>TODO: How to handle scopes?</p>
   * 
   * <p>TODO: This is just a basic implementation that is not
   * supposed to handle every possible configuration state. For
   * now this is the modifiable connection definitions only.</p>
   * 
   * @return
   */
  Map /*<String, Document>*/ prepareDocuments()
  {
	 HashMap docs = new HashMap();
	 
	 return docs;
  }
  
  void addConnectionDefinition(Scope scope, Node node) throws KeyUnavailableException
  {
    ConnectionDefinition p = new ConnectionDefinition(scope, node);
    String labelText = p.get(ConnectionDefinition.Key.LABEL);
    if(labelText == null) throw new IllegalStateException("Missing 'label' parameter within 'connection' node");

    if (defaultConnection == null)
      defaultConnection = labelText;
    
    connectionDefinitions.put(labelText, p);
  }
  
  /**
   * Returns a collection of all known {@link ConnectionDefinition} instances
   * which have been defined by the configuration documents.
   * 
   * @return
   */
  public Collection getConnectionDefinitions()
  {
    return connectionDefinitions.values();
  }
  
  /**
   * Returns a collection of {@link ConnectionDefinition} instances
   * which cannot be modified by program's user.
   * 
   * <p>Note: The current implementation marks every {@link ConnectionDefinition}
   * as fixed which was defined at site or installation scope.</p> 
   * 
   * @return
   */
  public Collection getFixedConnectionDefinitions()
  {
	  LinkedList ll = new LinkedList();
	  Iterator ite = connectionDefinitions.values().iterator();
	  while (ite.hasNext())
	  {
		ConnectionDefinition cd = (ConnectionDefinition) ite.next();
		if (cd.scope != Scope.USER)
		  ll.add(cd);
	  }
	  
	  return ll;
  }
  
  /**
   * Returns a collection of {@link ConnectionDefinition} instances
   * which can be modified by program's user.
   * 
   * <p>Note: The current implementation marks every {@link ConnectionDefinition}
   * as fixed which was defined at user scope.</p> 
   * 
   * @return
   */
  public Collection getModifiableConnectionDefinitions()
  {
	  LinkedList ll = new LinkedList();
	  Iterator ite = connectionDefinitions.values().iterator();
	  while (ite.hasNext())
	  {
		ConnectionDefinition cd = (ConnectionDefinition) ite.next();
		if (cd.scope == Scope.USER)
		  ll.add(cd);
	  }
	  
	  return ll;
  }
  
  public void setModifiableConnectionDefinitions(Collection c)
  {
	  // Remove former modifiable connection definitions.
	  Iterator ite = getModifiableConnectionDefinitions().iterator();
	  while (ite.hasNext())
	  {
		ConnectionDefinition cd = (ConnectionDefinition) ite.next();
		
		connectionDefinitions.remove(cd.get(ConnectionDefinition.Key.LABEL));
	  }
	  
	  ite = c.iterator();
	  while (ite.hasNext())
	  {
		ConnectionDefinition cd = (ConnectionDefinition) ite.next();
		cd.scope = Scope.USER;
		
		connectionDefinitions.put(cd.get(ConnectionDefinition.Key.LABEL), cd);
	  }
  }
  
  public ConnectionDefinition getDefaultConnectionDefinition()
  {
    return (ConnectionDefinition) connectionDefinitions.get(defaultConnection);
  }
  
  /**
   * Refer to {@link Appearance#put(String, Node)} for
   * documentation as the implementation is the same.
   * 
   * @param name
   * @param node
   */
  void put(String name, Node node) throws KeyUnavailableException
  {
    putParam(Key.getInstance(name), node);
  }

  /**
   * Refer to {@link Appearance#get(Appearance.Key)} for
   * documentation as the implementation is the same.
   * 
   * @param key
   * @param node
   */
  public String get(Key key)
  {
    return getParamValue(key, null);
  }
  
  /**
   * Refer to {@link Appearance#get(Appearance.Key, String)} for
   * documentation as the implementation is the same.
   * 
   * @param key
   * @param node
   */
  public String get(Key key, String defaultValue)
  {
    return getParamValue(key, defaultValue);
  }
  
  /**
   * Refer to {@link Appearance#getAsBoolean(Appearance.Key)} for
   * documentation as the implementation is the same.
   * 
   * @param key
   * @param node
   */
  public boolean getAsBoolean(Key key)
  {
    return getParamAsBoolean(key, false);
  }
  
  /**
   * Refer to {@link Appearance#getAsBoolean(Appearance.Key, boolean)} for
   * documentation as the implementation is the same.
   * 
   * @param key
   * @param node
   */
  public boolean getAsBoolean(Key key, boolean defaultValue)
  {
    return getParamAsBoolean(key, defaultValue);
  }

  /**
   * Refer to {@link Appearance#getAsInt(Appearance.Key)} for
   * documentation as the implementation is the same.
   * 
   * @param key
   * @param node
   */
  public int getAsInt(Key key)
  {
    return getParamAsInt(key, 0);
  }
  
  /**
   * Refer to {@link Appearance#getAsInt(Appearance.Key, int)} for
   * documentation as the implementation is the same.
   * 
   * @param key
   * @param node
   */
  public int getAsInt(Key key, int defaultValue)
  {
    return getParamAsInt(key, defaultValue);
  }
  
  /**
   * Refer to {@link Appearance#getAsInt(Appearance.Key, int)} for
   * documentation as the implementation is the same.
   * 
   * @param key
   * @param node
   */
  
  public long getAsLong(Key key, long defaultValue)
  {
	  return getParamAsLong(key, defaultValue);
  }
  
  /**
   * Returns if the environment configuration settings may
   * be overridden by the user.
   * 
   * <p>By default this method returns <code>true</code>. This
   * can be changed by the installation and site scope environment
   * configuration document.</p>
   * 
   * @return true if users are allowed to overwrite environment settings.
   */
  
  public boolean isUserOverridable()
  {
  	return userOverridable;
  }
  
  void setUserOverridable(String value)
  {
    userOverridable = getAsBoolean(value, true);
  }
  
  /**
   * This class provide the funtionality for defining
   * configuration keys for the environment configuration
   * and provides a type-safe enumeration.
   * 
   * <p>For general information about the configuration system
   * refer to the documentation on the project's website.</p>
   * 
   * <p>You are supposed to subclass this class, implement
   * {@link #Key(String)} and define your configuration keys
   * as <code>public static final</code> constants.</p> 
   * 
   * @author Robert Schuster
   *
   */
  public static abstract class Key extends Base.Key
  {

    /**
     * Refer to the {@link Appearance.Key#instances} documentation
     * for detailed documentation as the information applies here,
     * too.
     */
    private static HashMap instances = new HashMap();

    protected Key(String label)
    {
      super(label);
      instances.put(label, this);
    }
    
    /** Returns an instance of this class or throws
     * a {@KeyUnavailableException} if it does not exist.
     * 
     * @param label
     * @return
     * @throws KeyUnavailableException if the key does not exist.
     */
    private static Key getInstance(String label)
      throws KeyUnavailableException
    {
      Key k = (Key) instances.get(label);
      
      if (k == null)
        throw new KeyUnavailableException(label);
      
      return k;
    }
    
  }
  
}
