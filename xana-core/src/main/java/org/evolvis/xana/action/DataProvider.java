package org.evolvis.xana.action;

public interface DataProvider {
	
	public void registerDataConsumer(Object object);

}
