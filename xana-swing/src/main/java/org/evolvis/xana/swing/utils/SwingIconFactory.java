

package org.evolvis.xana.swing.utils;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.evolvis.xana.utils.AbstractIconFactory;

public class SwingIconFactory extends AbstractIconFactory
{
	private static SwingIconFactory instance = new SwingIconFactory();

	private SwingIconFactory()
	{
		// Do not instantiate
	}

	/** Returns Icon Factory. */
	public static SwingIconFactory getInstance()
	{
		return instance;
	}

	/**
	 * @see org.evolvis.xana.utils.AbstractIconFactory#getImageObjectFromURL(java.net.URL)
	 */
	@Override
	protected Object getImageObjectFromURL(URL loadURL) throws IOException {
		BufferedImage bufferedImage = ImageIO.read(loadURL);

		if(bufferedImage != null)
			return new ImageIcon(bufferedImage);
		
		return null;
	}
}
