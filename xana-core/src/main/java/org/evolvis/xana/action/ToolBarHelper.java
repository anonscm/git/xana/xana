/**
 * 
 */
package org.evolvis.xana.action;

import java.util.logging.Logger;

import javax.swing.Action;

/**
 * This class provides some helper methods for ToolBar-implementations
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ToolBarHelper {

	private static final Logger logger = Logger.getLogger(ToolBarHelper.class.getName());
    
    /** The key to store and retrieve the assigned group name via {@link Action#putValue()} 
     */
    public static final String GROUP_NAME = "toolBar.groupName"; 

    /** The key to store and retrieve the assigned group priority via {@link Action#putValue()} 
     */
    public static final String GROUP_PRIORITY = "toolBar.groupPriority"; 

    /** The key to store and retrieve the assigned action priority via {@link Action#putValue()} 
     */
    public static final String ACTION_PRIORITY = "toolBar.actionPriority"; 

    //fix minor priority as default 
    public static final int DEFAULT_PRIORITY = Integer.MAX_VALUE;
    //fix default group name
    public static final String DEFAULT_GROUP = "default";
    //fix offset between groups
    public static final int GROUP_OFFSET = 9;
    
    /** Throws ActionContainerException if an action has invalid type: 
     * <li> Choice Box, </li>
     * <li> or Separator. </li>
     */
    public static void checkType( Action action ) throws ActionContainerException {
        String actionType = (String) action.getValue(AbstractGUIAction.PROP_KEY_ACTION_TYPE);
        
        if (AbstractGUIAction.TYPE_CHOISE.equals(actionType))
            throw new ActionContainerException("illegally defined a radio button for the tool bar");
        
        else if (AbstractGUIAction.TYPE_SEPARATOR.equals(actionType))
            throw new ActionContainerException("illegally defined a separator for the tool bar");
    }
	
	/**
     * Four cases are possible:
     * <li> "" empty path,
     * <li> "groupName" or "actionPriority" only,
     * <li> "groupName:actionPriority" or
     * <li> "groupName:groupPriority:actionPriority" as full path.
     */
    public static void checkPath( Action action, String path ) {
        if(path == null || "".equals(path)) {
            logger.fine("[!] no path: last position will be used as default");
            return;
        }

        //check valid format
        String[] pathData = path.split(":");
        if(pathData.length > 3 || pathData.length <= 0) {
            logger.fine("[!] invalid action path format [" + pathData + "]: last position will be used as default");
            return;
        }
        //retrieve data
        switch ( pathData.length ) {
            case 1:
                try{
                    Integer.valueOf(pathData[0]);
                    //is action priority
                    action.putValue(ACTION_PRIORITY, pathData[0]);
                }catch(NumberFormatException e){
                    //is group name
                    action.putValue(GROUP_NAME, pathData[0]);
                }
                break;
            case 2:
                action.putValue(GROUP_NAME, pathData[0]);
                action.putValue(ACTION_PRIORITY, pathData[1]);
                break;
            case 3:
                action.putValue(GROUP_NAME, pathData[0]);
                action.putValue(GROUP_PRIORITY, pathData[1]);
                action.putValue(ACTION_PRIORITY, pathData[2]);
                break;
        }
    }
}
