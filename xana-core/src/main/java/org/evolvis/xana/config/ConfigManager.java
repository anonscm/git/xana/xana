

package org.evolvis.xana.config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * The <code>ConfigManager</code> handles the configuration loading mechanism
 * and provides accessor methods to the environment and appearance
 * configuration.
 * <p>
 * For a general introduction to the configuration system refer to the
 * respective document on the project's website.
 * </p>
 * <p>
 * An instance of this class can be acquired by calling {@link #getInstance()}.
 * It is only needed to start the configuration loading step.
 * </p>
 * <p>
 * When the configuration has been loaded one can get the appearance
 * configuration via {@link #getAppearance()} and the environment configuration
 * via {@link #getEnvironment()}.
 * @author Robert Schuster
 */
public class ConfigManager
{
  private static Logger logger = Logger.getLogger(ConfigManager.class.getName());

  private static ConfigManager instance;
  
  // Configuration files
  private static final String APPEARANCE_CONFIG = "appearance.xml";
  
  private static final String ENVIRONMENT_CONFIG = "environment.xml";
  
  /** A list of document which failed to parse. */
  private List/*<String>*/ missingDocuments;

  /**
   * The loader instance which is used to retrieve the configuration documents.
   */
  private Loader loader;

  /**
   * Base system property key for the bootstrap configuration.
   */
  private String bootstrapBase = "org.evolvis.xana.config";

  /**
   * System property key for the bootstrap configuration type.
   */
  private String bootstrapType = bootstrapBase + ".type";

  /**
   * System property value denoting that configuration documents are to be
   * retrieved from the local filesystem.
   * <p>
   * This is the default if the bootstrap configuration does not contain
   * explicit information about this.
   * </p>
   */
  private static final String BOOTSTRAP_TYPE_FILE = "file";

  /**
   * System property value denoting that configuration documents are to be
   * retrieved from a jar file.
   */
  private static final String BOOTSTRAP_TYPE_JAR = "jar";
  
  /**
   * System property key for the application variant.
   */
  private String bootstrapVariant = bootstrapBase + ".variant";

  /**
   * System property value for the default application variant: "default".
   */
  private static final String BOOTSTRAP_VARIANT_DEFAULT = "default";

  /**
   * Name of the root tag in the appearance configuration documents.
   */
  private static final String ROOT_TAG_APPEARANCE = "appearance";

  /**
   * Name of the root tag in the environment configuration documents.
   */
  private static final String ROOT_TAG_ENVIRONMENT = "environment";

  /**
   * Name of the root tag in the action definition documents.
   */
  private static final String ROOT_TAG_ACTIONS = "actions";

  /**
   * Name of the root tag in the plugin definition documents.
   */
  private static final String ROOT_TAG_PLUGINS = "plugins";

  /**
   * Name of the root tag in the connection definition documents.
   */
  private static final String ROOT_TAG_CONNECTIONS = "connections";
  
  /** Preferences node name of personal application-settings.*/
  private String prefBaseNodeName;
  
  private Class applicationClass;
  
  private String variant;

  private Appearance ape = new Appearance();

  private Environment env = new Environment();
  
  private Preferences prefs;

  private ConfigManager()
  {
  }
  
  public void setPrefBaseNodeName(String prefBaseNodeName) {
	  this.prefBaseNodeName = prefBaseNodeName;
  }
  
  public void setBootstrapVariant(String bootstrapVariant) {
	  this.variant = bootstrapVariant;
  }
  
  /**
   * Sets the application class on which it is later on decided
   * whether the config system is used in a development or
   * production mode.
   * 
   * <p>You can provide the <code>Class</code> instance of any
   * application class that is not in a jar at development time.</p>
   * 
   * <p>Use this method before any of the {@link #init} methods are
   * called, otherwise it has no effect.</p> 
   *  
   * @param applicationClass
   */
  public void setApplicationClass(Class applicationClass) {
	  this.applicationClass = applicationClass;
  }

  /**
   * Invokes {@link #init(String, String)} with the first argument
   * being <code>null</code>. See the other methods' documentation for
   * details. 
   * 
   * @param applicationId
   * @throws IllegalStateException
   */
  public void init(String applicationId) throws IllegalStateException
  {
    init(bootstrapBase, applicationId);
  }

  /**
   * Initializes the configuration system by evaluating the bootstrap
   * configuration and loading the configuration documents.
   * <p>
   * After this step the instances returned by {@link #getAppearance()} and
   * {@link #getEnvironment()} contain meaningful configuration data which can
   * be used throughout the application.
   * </p>
   * 
   * <p>
   * If no appearance configuration can be loaded an {@link IllegalStateException}
   * is thrown.
   * </p>
   * 
   * <p>By providing a non-null bootstrap base argument one can define a different
   * base name for the system properties which provide the config system's bootstrap
   * information. The default base is
   * <code>org.evolvis.xana.config.bootstrap</code> and this value is used when
   * the first argument to this method is <code>null</code>.</p>
   * 
   * @param bootstrapBase Base name for the system properties providing the config
   *                      system's boot information.
   * @param applicationId A short identifier for the application using the config system.
   *                      This will be used for default file locations etc.
   * @throws IllegalStateException
   *           when an important configuration document cannot be found or
   *           parsed.
   */
  public void init(String bootstrapBase, String applicationId) throws IllegalStateException
  {
    if (bootstrapBase != null) {
      this.bootstrapBase = bootstrapBase;
      bootstrapType = bootstrapBase + ".type";
      bootstrapVariant = bootstrapBase + ".variant";
    }

    String type = System.getProperty(bootstrapType);
    if (type == null)
      {
        logger.warning("Sytem property " + bootstrapType + " is not set. Using default: " + BOOTSTRAP_TYPE_FILE);
        type = BOOTSTRAP_TYPE_FILE;
      }
    
    // If a variant is given via system properties, take the value from it. Even if that possibly overrides
    // a hardcoded value from the appplication.
    if(System.getProperty(bootstrapVariant) != null)
    	variant = System.getProperty(bootstrapVariant);
    
    // Still no variant, take the config system's default value.
    if (variant == null) {
        logger.warning("System property " + bootstrapVariant + " is not set. Using default: " + BOOTSTRAP_VARIANT_DEFAULT);
        variant = BOOTSTRAP_VARIANT_DEFAULT;
    }
    
    loadConfiguration(applicationId, type, variant);
  }

  public static Appearance getAppearance()
  {
    return getInstance().ape;
  }

  public static Environment getEnvironment()
  {
    return getInstance().env;
  }
  
  public static Preferences getPreferences()
  {
    return getInstance().prefs;
  }
  
  public void store()
  {
	  Map/*<String, Document>*/ documents = getEnvironment().prepareDocuments();
	  
	  Iterator ite = documents.entrySet().iterator();
	  while (ite.hasNext())
	  {
		  Map.Entry entry = (Map.Entry) ite.next();
		  String name = (String) entry.getKey();
		  Document doc = (Document) entry.getValue();
		  
		  try
		    {
		      loader.storeDocument(Scope.USER, name, doc);
		    }
		  catch (DocumentUnavailableException e)
		    {
			  logger.warning("storing of document failed: " + e.docURL);
		    }
	  }
  }
  
  /**
   * Returns a list of documents which failed to parse and are therefore
   * missing.
   * 
   * <p>The list is empty of no documents are missing.</p>
   * 
   * @return
   */
  public static List getMissingDocuments()
  {
	  ConfigManager instance = getInstance();
	  if (instance.missingDocuments == null)
		return Collections.EMPTY_LIST;
    
	  return new ArrayList(instance.missingDocuments);
  }

  private void loadConfiguration(String applicationId, String configType, String variant)
      throws IllegalStateException
  {
    // Instantiates the Loader instance to be used for retrieving the
    // configuration documents.
    if (configType.equals(BOOTSTRAP_TYPE_FILE))
      loader = new FileLoader(bootstrapBase, variant, applicationId, applicationClass);
    else if (configType.equals(BOOTSTRAP_TYPE_JAR))
      loader = new JARLoader(bootstrapBase, variant, applicationId, applicationClass);
    
    // Initializes the application variant's Preferences instance.
    prefs = Preferences.userRoot().node(prefBaseNodeName + "/" + variant);

    // Loads the appearance configuration from site and installation scope.
    Document doc = null;
    Handler handler = new AppearanceHandler();
    String rootTag = ROOT_TAG_APPEARANCE;
    boolean appearanceConfigAvailable = false;

    try
      {
        doc = loader.getDocument(Scope.SITE, APPEARANCE_CONFIG);
        insertElements(Scope.SITE, doc, rootTag, handler);
        appearanceConfigAvailable = true;
      }
    catch (DocumentUnavailableException e)
      {
    	addMissingDocument(e.docURL);  // site-scope appearance config is mandatory
        logger.warning(
                             "appearance configuration at site scope not available: "
                                 + e.getReason());
      }

    try
      {
        doc = loader.getDocument(Scope.INSTALLATION,
                                 APPEARANCE_CONFIG);
        insertElements(Scope.INSTALLATION, doc, rootTag, handler);
      }
    catch (DocumentUnavailableException e)
      {
    	// installation-scope apprearance config is optional
        logger.warning(
                             "appearance configuration at installation scope not available: "
                                 + e.getReason());

        if (!appearanceConfigAvailable)
          throw new IllegalStateException(
                                          "No appearance configuration available at all.");
      }

    rootTag = ROOT_TAG_ENVIRONMENT;
    handler = new EnvironmentHandler();
    try
      {
        // Loads the environment configuration from site, installation and user
        // scope.
        doc = loader.getDocument(Scope.SITE,
                                 ENVIRONMENT_CONFIG);
        insertElements(Scope.SITE, doc, rootTag, handler);
      }
    catch (DocumentUnavailableException e)
      {
    	addMissingDocument(e.docURL);  // site-scope appearance config is mandatory
        logger.warning(
                             "environment configuration at site scope not available: "
                                 + e.getReason());

      }

    try
      {
        doc = loader.getDocument(Scope.INSTALLATION,
                                 ENVIRONMENT_CONFIG);
        insertElements(Scope.INSTALLATION, doc, rootTag, handler);
      }
    catch (DocumentUnavailableException e)
      {
    	// installation-scope environment config is optional
    	addMissingDocument(e.docURL);
        logger.warning(
                             "environment configuration at installation scope not available: "
                                 + e.getReason());

      }

    // Prevents trying to read the user scope environment configuration document.
    if (!env.isUserOverridable())
      {
        logger.info("user scope environment configuration disabled.");
        return;
      }
    
    try
      {
        doc = loader.getDocument(Scope.USER,
                                 ENVIRONMENT_CONFIG);
        insertElements(Scope.USER, doc, rootTag, handler);
      }
    catch (DocumentUnavailableException e)
      {
    	// user-scope environment config is optional
        logger.warning(
                             "environment configuration at user scope not available: "
                                 + e.getReason());

      }

  }

  private void insertElements(Scope scope, Document doc, String rootTag,
                              Handler handler) throws DocumentUnavailableException
  {
    NodeList list = doc.getElementsByTagName(rootTag).item(0).getChildNodes();
    int size = list.getLength();

    for (int i = 0; i < size; i++)
      {
        Node n = list.item(i);
        NamedNodeMap attributes = n.getAttributes();

        try
          {
            handler.handle(scope, n, n.getNodeName(), attributes);
          }
        catch (KeyUnavailableException e)
          {
            logger.warning("The " + rootTag + " configuration of scope "
                           + scope + " contains the unknown key: "
                           + e.getKeyLabel());
          }
        catch (ParseException pe)
        {
          logger.warning(pe.getMessage());
        }

      }
  }
  
  /**
   * Reads a document with the given loader from the given scope. The method
   * is supposed to be used with secondary documents only whose absence (e.g. 
   * due to errors) is tolerable.
   * 
   * <p>In case the loading fails an error message is logged and <code>null</code>
   * is returned. This gives the callee the possibility to continue parsing.
   * Additionally the document's name is added to the list of missing documents
   * which can be accessed later to show a proper diagnostic dialog.</p>
   * 
   * @param loader
   * @param scope
   * @param docName
   * @return
   */
  private Document getLinkedDocument(Loader loader, Scope scope, String docName)
  {
    try
    {
    	return loader.getDocument(scope, docName);
    }
    catch (DocumentUnavailableException e)
    {
    	addMissingDocument(e.docURL);
    	logger.warning("linked document from scope \"" + scope + "\" is not available: " + e.reason);
    	
    	return null;
    }
  }
  
  private void addMissingDocument(String docURL)
  {
  	if (missingDocuments == null)
  	  missingDocuments = new ArrayList();
  	
  	missingDocuments.add(docURL);
  }

  public static ConfigManager getInstance()
  {
    // Try to avoid synchronized block.
    if (instance != null)
      return instance;

    synchronized (ConfigManager.class)
      {
        if (instance == null)
          return instance = new ConfigManager();

        return instance;
      }

  }

  /**
   * Abstract base class for document loaders.
   * <p>
   * A <code>Loader</code> implementation encapsulates the way configuration
   * documents are retrieved.
   * </p>
   * <p>
   * Additionally the implementation controls which system properties control it
   * and where the documents are located in the different scopes.
   * </p>
   * <p>
   * For a general introduction to the configuration system refer to the
   * respective document on the project's website.
   * </p>
   * <p>
   * Subclass instances have access to the constant field <code>VARIANT</code>
   * which denotes the application variant.
   * </p>
   * @author Robert Schuster
   */
  static abstract class Loader
  {
    protected final String BASE;
    
    protected final String VARIANT;

    protected Loader(String base, String variant)
    {
      BASE = (base == null ? "de.tarent.commons.config" : base);
      VARIANT = variant;
    }

    protected abstract Document getDocument(Scope scope, String docName)
        throws DocumentUnavailableException;
    
    protected abstract boolean isStoringSupported(Scope scope);

    protected void storeDocument(Scope scope, String docName, Document doc)
        throws DocumentUnavailableException, UnsupportedOperationException
    {
      throw new UnsupportedOperationException();	
    }

  }

  static class DocumentUnavailableException extends Exception
  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1311824187433589069L;
	String docURL;
    String reason;

    DocumentUnavailableException(String docURL, String reason)
    {
      super();
      this.docURL = docURL;
      this.reason = reason;
    }

    String getReason()
    {
      return reason;
    }
  }
  
  static class ParseException extends Exception
  {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6643357532451708370L;

	ParseException(String msg)
    {
      super(msg);
    }
    
    ParseException(String msg, Throwable cause)
    {
      super(msg, cause);
    }
    
  }

  /**
   * Typesafe enumeration whose instances denote the configuration scopes.
   * <p>
   * The constant instance of this class are needed for {@link Loader}
   * implementations.
   * </p>
   * <p>
   * For a general introduction to the configuration system refer to the
   * respective document on the project's website.
   * </p>
   */
  static class Scope
  {
    private String label;

    static final Scope SITE = new Scope("site");

    static final Scope INSTALLATION = new Scope("installation");

    static final Scope USER = new Scope("user");

    private Scope(String label)
    {
      this.label = label;
    }

    public boolean equals(Object o)
    {
      return o instanceof Scope && label == ((Scope) o).label;
    }

    public String toString()
    {
      return label;
    }
  }

  private interface Handler
  {
    void handle(Scope scope, Node node, String nodeName, NamedNodeMap attributes)
        throws KeyUnavailableException, DocumentUnavailableException, ParseException;
  }

  private class AppearanceHandler implements Handler
  {

    public void handle(Scope scope, Node node, String nodeName,
                       NamedNodeMap attributes) throws KeyUnavailableException,
                       DocumentUnavailableException, ParseException
    {
      if (nodeName == null)
        {
          return;
        }
      else if (nodeName.equals("param"))
        {
          Node name = attributes.getNamedItem("name");
          
          if (name == null)
            throw new ParseException("expected name attribute in <param> tag");

          ape.put(name.getNodeValue(), node);

        }
      else if (nodeName.equals("include"))
        {
          Node documentName = attributes.getNamedItem("file");
          
          if (documentName == null)
            throw new ParseException("expected file attribute in <include> tag");

          
          Document linkedDoc = getLinkedDocument(loader, scope, documentName.getNodeValue());
          if (linkedDoc != null)
            insertElements(scope, linkedDoc, ROOT_TAG_APPEARANCE, this);
        }
      else if (nodeName.equals("actions"))
        {
          Node documentName = attributes.getNamedItem("file");

          if (documentName == null)
            throw new ParseException("expected file attribute in <actions> tag");
          
          Document actionsDoc = getLinkedDocument(loader, scope, documentName.getNodeValue());
          
          if (actionsDoc != null)
            insertElements(scope, actionsDoc, ROOT_TAG_ACTIONS,
                         new ActionsHandler());
        }
      else if (nodeName.equals("plugins"))
        {
          Node documentName = attributes.getNamedItem("file");

          if (documentName == null)
            throw new ParseException("expected file attribute in <plugins> tag");

          Document pluginsDoc = getLinkedDocument(loader, scope, documentName.getNodeValue());
          
          if (pluginsDoc != null)
          insertElements(scope, pluginsDoc, ROOT_TAG_PLUGINS,
                         new PluginsHandler());
        }
    }

  }

  private class ActionsHandler implements Handler
  {
    public void handle(Scope scope, Node node, String nodeName,
                       NamedNodeMap attributes) throws KeyUnavailableException,
                       DocumentUnavailableException, ParseException
    {
      if (nodeName.equals("action"))
        {
          String key = getId(node);
          try
          {
            ape.addActionDefinition(key, node);
          }
          catch (DataFormatException dfe)
          {
            throw new ParseException("Error parsing action definition " + key, dfe);
          }
        }
      else if (nodeName.equals("include"))
        {
          Node documentName = attributes.getNamedItem("file");

          if (documentName == null)
            throw new ParseException("expected file attribute in <include> tag");

          Document actionsDoc = getLinkedDocument(loader, scope, documentName.getNodeValue());
          
          if (actionsDoc != null)
            insertElements(scope, actionsDoc, ROOT_TAG_ACTIONS, this);
        }
    }
    
    String getId(Node action) throws ParseException
    {
      NodeList params = action.getChildNodes();
      int size = params.getLength();

      for (int i = 0; i < size; i++)
        {
          Node param = params.item(i);
          NamedNodeMap attributes = param.getAttributes();
          if (attributes == null)
            continue;

          Node attr = attributes.getNamedItem("name");
          if (attr != null && attr.getFirstChild() != null && attr.getFirstChild().getNodeValue().equals("UniqueName"))
          {
        	  Node attrVal = attributes.getNamedItem("value");
        	  if(attrVal != null && attrVal.getFirstChild() != null)
        		  return attrVal.getFirstChild().getNodeValue();
          }
        }

      throw new ParseException("expected a <param> tag with an name attribute and a value of \"UniqueName\" within an <action> tag");
    }

  }

  private class PluginsHandler implements Handler
  {
    public void handle(Scope scope, Node node, String nodeName,
                       NamedNodeMap attributes) throws KeyUnavailableException,
                       DocumentUnavailableException, ParseException
    {
      if (nodeName.equals("plugin"))
        {
          String key = getId(node);
          ape.addPluginDefinition(key, node);
        }
      else if (nodeName.equals("include"))
        {
          Node documentName = attributes.getNamedItem("file");

          if (documentName == null)
            throw new ParseException("expected file attribute in <include> tag");

          Document includeDoc = getLinkedDocument(loader, scope, documentName.getNodeValue());
          
          if (includeDoc != null)
            insertElements(scope, includeDoc, ROOT_TAG_PLUGINS, this);
        }
    }
    
    String getId(Node action) throws ParseException
    {
      NodeList params = action.getChildNodes();
      int size = params.getLength();

      for (int i = 0; i < size; i++)
        {
          Node param = params.item(i);
          NamedNodeMap attributes = param.getAttributes();
          if (attributes == null)
            continue;

          Node attr = attributes.getNamedItem("name");
          if (attr != null && attr.getFirstChild() != null && attr.getFirstChild().getNodeValue().equals("id"))
          {
            Node attrVal = attributes.getNamedItem("value");
            
            if(attrVal != null && attrVal.getFirstChild() != null)
              return attrVal.getFirstChild().getNodeValue();
          }
        }

      throw new ParseException("expected a <param> tag with an value attribute within a <plugin> tag");
    }

  }

  private class EnvironmentHandler implements Handler
  {

    public void handle(Scope scope, Node node, String nodeName,
                       NamedNodeMap attributes) throws KeyUnavailableException,
                       DocumentUnavailableException, ParseException
    {
      if (nodeName == null)
        {
          return;
        }
      else if (nodeName.equals("param"))
        {
          Node name = attributes.getNamedItem("name");

          env.put(name.getNodeValue(), node);
        }
      else if (nodeName.equals("include"))
        {
          Node documentName = attributes.getNamedItem("file");

          if (documentName == null)
            throw new ParseException("expected file attribute in <include> tag");

          Document linkedDoc = getLinkedDocument(loader, scope, documentName.getNodeValue());
          
          if (linkedDoc != null)
            insertElements(scope, linkedDoc, ROOT_TAG_ENVIRONMENT, this);
        }
      else if (nodeName.equals("connections"))
        {
          Node documentName = attributes.getNamedItem("file");

          if (documentName == null)
            throw new ParseException("expected file attribute in <include> tag");

          Document includeDoc = getLinkedDocument(loader, scope, documentName.getNodeValue());
          
          if (includeDoc != null)
            insertElements(scope, includeDoc, ROOT_TAG_CONNECTIONS, new ConnectionsHandler());
          
        }
      else if (nodeName.equals("userOverridable"))
        {
          // Detects and handles the userOverridable tag which is only allowed in 
          // site and installation scope.
          if (scope == Scope.USER)
            throw new ParseException("<userOverridable> tag not allowed in user scope configuration document.");
          
          Node name = attributes.getNamedItem("value");

          env.setUserOverridable(name.getNodeValue());
          
        }
    }

  }
  
  private class ConnectionsHandler implements Handler
  {
    public void handle(Scope scope, Node node, String nodeName,
                       NamedNodeMap attributes) throws KeyUnavailableException,
                       DocumentUnavailableException, ParseException
    {
      if (nodeName.equals("connection"))
        {
          env.addConnectionDefinition(scope, node);
        }

    }

  }

}
