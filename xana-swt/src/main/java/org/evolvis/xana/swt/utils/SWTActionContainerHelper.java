/**
 * 
 */
package org.evolvis.xana.swt.utils;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Logger;

import javax.swing.Action;

import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.ToolItem;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SWTActionContainerHelper {
	
	protected final static Logger logger = Logger.getLogger(SWTActionContainerHelper.class.getName());

	public static void addEnabledListener(final Item item, Action action) { 
		
		action.addPropertyChangeListener(new PropertyChangeListener() {

			/**
			 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
			 */
			public void propertyChange(final PropertyChangeEvent event) {
				
				if(event.getPropertyName().equals("enabled"))
					item.getDisplay().asyncExec(new Runnable() {

						/**
						 * @see java.lang.Runnable#run()
						 */
						public void run() {
							if(item instanceof ToolItem)
								((ToolItem)item).setEnabled((Boolean)event.getNewValue());
							else if(item instanceof MenuItem)
								((MenuItem)item).setEnabled((Boolean)event.getNewValue());
							else
								logger.warning("Unknown Item-type: "+item.getClass());
						}
						
					});
			}
			
		});
	}
}

