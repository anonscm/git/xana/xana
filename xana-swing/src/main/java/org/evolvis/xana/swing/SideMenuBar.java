/* $Id: SideMenuBar.java,v 1.15 2007/08/30 16:10:29 fkoester Exp $
 *
 * tarent-contact, Plattform-Independent Webservice-Based Contactmanagement
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-contact'
 * (which makes passes at compilers) written
 * by Sebastian Mancke.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */


package org.evolvis.xana.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.basic.BasicButtonUI;

import org.evolvis.xana.action.AbstractGUIAction;
import org.evolvis.xana.action.ActionContainer;
import org.evolvis.xana.action.ActionContainerException;
import org.evolvis.xana.action.ActionRegistry;
import org.evolvis.xana.action.MenuHelper;
import org.evolvis.xana.action.TarentGUIAction;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;


/**
 * Implementation of the SideMenubar as an ActionContainer. This implementation
 * follows the example of de.tarent.contact.gui.action.MenuBar.
 */
public class SideMenuBar extends JPanel implements ActionContainer
{
  /**
	 * 
	 */
	private static final long serialVersionUID = 4037609367183325761L;

protected Map menus = new HashMap();

  protected String uniqueName;

  /** The background color for the menu.
   */
  private static Color SIDEMENU_MENU_BACKGROUND;

  /** The border for the menu.
   */
  private static Border SIDEMENU_MENU_BORDER;

  /** The background color for the menu items.
   */
  private static Color SIDEMENU_ITEM_BACKGROUND;
  
  /** The border for the menu items.
   */
  private static Border SIDEMENU_ITEM_BORDER;
  
  /** Just a unique String which identifies the priority key
   * in the <code>Action</code> instance.
   */
  private static String PRIORITY_KEY = "sidemenu.items.priority.key";

  /**
   * Fixed alignment value for the CollapsePaneMenu instances and the rigid
   * areas between them.
   */
  private final float ALIGNMENT = Component.LEFT_ALIGNMENT;
  
  /**
   * Denotes the row index where to insert the next menu component.
   *
   * <p>This is updated after a menu has been inserted.</p>
   */
  private int row = 2;

  private FormLayout layout;
  
  protected final static Logger logger = Logger.getLogger(SideMenuBar.class.getName());

  public SideMenuBar(String uniqueName)
  {
    this.uniqueName = uniqueName;
    
    if (SIDEMENU_ITEM_BORDER == null)
      {
        UIDefaults defs = UIManager.getLookAndFeelDefaults();
        SIDEMENU_ITEM_BACKGROUND = defs.getColor("tarent.sidemenu.item.background");
        SIDEMENU_ITEM_BORDER = defs.getBorder("tarent.sidemenu.item.border");
        
        SIDEMENU_MENU_BACKGROUND = defs.getColor("tarent.sidemenu.menu.background");
        SIDEMENU_MENU_BORDER = defs.getBorder("tarent.sidemenu.menu.border");
      }

    layout = new FormLayout("3dlu, fill:pref:grow, 3dlu");
    setLayout(layout);
  }

  /**
   * Returns container's unique name.
   */
  public String getContainerUniqueName()
  {
    return uniqueName;
  }

  /**
   * Adds the given action to a menu bar at assigned position.
   * @param action
   *          The action to attach
   * @param menuPath
   *          The path and rules to attach, as described above
   */
  public void attachGUIAction(Action action, String menuPath)
  {
    AbstractButton item;

    String actionType = (String) action.getValue(AbstractGUIAction.PROP_KEY_ACTION_TYPE);

    if (AbstractGUIAction.TYPE_CHECK.equals(actionType))
      {
        item = new JCheckBox(action);
        // Adds the checkbox to the list of elements whose selection state
        // can be set through AbstractGUIAction.setSelected(boolean)
        MenuHelper.addSynchronizationComponent(action, (JToggleButton) item);
      }
    else if (AbstractGUIAction.TYPE_CHOISE.equals(actionType))
        item = new JRadioButton(action);
    else if (AbstractGUIAction.TYPE_SEPARATOR.equals(actionType))
      {
        logger.warning("[!] illegally defined a separator for the side menu");
        return;
      }
    else
        item = new JButton(action);

    // Make buttons less intrusive and add a small offset of the text to
    // the component's edge.
    item.setBackground(SIDEMENU_ITEM_BACKGROUND);
    item.setBorder(SIDEMENU_ITEM_BORDER);
    
    // set component name (for later ecxeption handling)
    item.setName((String) action.getValue(AbstractGUIAction.NAME));
    item.setMnemonic(-1);//disable mnemonic key (with unknown key)
    item.setHorizontalAlignment(SwingConstants.LEADING);
    item.setAlignmentX(Component.LEFT_ALIGNMENT);
    
    addItem(item, menuPath, action);
  }

  public void removeGUIAction(Action action)
  {
    logger.warning("optional, not implemented method yet: removeGUIAction");
  }
  
  public void initActions() throws ActionContainerException {
  	Iterator sbIt = ActionRegistry.getInstance().getActions(getContainerUniqueName()).iterator();
		
		while(sbIt.hasNext())
		{
			TarentGUIAction action = (TarentGUIAction)sbIt.next();
			attachGUIAction(action, action.getMenuPath(getContainerUniqueName()));
		}
  }

  /**
   * @param item
   *          The item to attach
   * @param menuPath
   *          The path and rules to attach, as described above
   * @param withBackSeparator
   */
  private void addItem(AbstractButton item, String menuPath, Action action)
  {
    CollapsePaneMenu currentMenu = getAssignedMenu(menuPath);
    
    int priority = MenuHelper.getAssignedPriority(menuPath);
    action.putValue(PRIORITY_KEY, new Integer(priority));
    
    int pos = MenuHelper.getInsertPosition(currentMenu.getComponents(),
                                           PRIORITY_KEY,
                                           new Integer(priority));
    
    currentMenu.add(item, pos++);
  }
  
  private CollapsePaneMenu getAssignedMenu(String menuPath)
  {
    String menuName = menuPath.split(":")[0];
    CollapsePaneMenu parentMenu = null;
    CollapsePaneMenu currentMenu = (CollapsePaneMenu) menus.get(menuName);

    if (null == currentMenu)
      {
        currentMenu = new CollapsePaneMenu(menuName);
        currentMenu.setAlignmentX(ALIGNMENT);

        fixMenuName(currentMenu);

        menus.put(menuName, currentMenu);

        if (null != parentMenu)
            parentMenu.add(currentMenu);
        else
          {
            layout.appendRow(new RowSpec("6dlu"));
            layout.appendRow(new RowSpec("pref"));
            CellConstraints cc = new CellConstraints();
            add(currentMenu, cc.xy(2, row));
            row += 2;
          }
      }
    parentMenu = currentMenu;

    return currentMenu;
  }

  /** Replaces underscores with spaces in the menu title.
   * 
   * @param currentMenu
   */
  private void fixMenuName(CollapsePaneMenu currentMenu)
  {
    String menuNameToParse = currentMenu.getText();
    if (menuNameToParse.indexOf("_") > 0)
      {
        currentMenu.setText(menuNameToParse.replace('_', ' '));
      }
  }
  
  /**
   * CollapsePaneMenu is a menu based on the {@link CollapsePane} component.
   * 
   * <p>It allows displaying common Swing components in a menu like manner.</p>
   * 
   * <p>To add menu items to an instance use the {@link add(AbstractButton, int)} method.
   * This will automatically handle updating the component's UI. The UI change is
   * neccessary to have the 'rollover effect'. The implementation takes the components
   * <b>current</b> UI to handle the old and the new behavior.</p>
   * 
   * <p>Similar to {@link javax.swing.JMenu} there is a method {@link addSeparator}
   * which inserts a horizontal bar between adjacent menu items.</p>.
   * 
   * @author Robert Schuster
   *
   */
  private static class CollapsePaneMenu extends CollapsePane
  {
    /**
	 * 
	 */
	private static final long serialVersionUID = 2216948360387117564L;
	private static RolloverListener rolloverListener = new RolloverListener();
    
    /** Adds a clickable item to the menu.
     * 
     * <p>The methods changes the item's UI in order to make painting
     * of a rollover effect possible. The implementation should be
     * flexible enough to handle all kinds of <code>AbstractButton</code>
     * implementations.</p> 
     * 
     * <p>Note: Other <code>add</code> variants have not been written
     * because their is no need for them yet.</p>
     * 
     * @param b
     * @param index
     * @return
     */
    public AbstractButton add(final AbstractButton b, int index)
    {
      b.setUI(new RolloverUI(b.getUI()));
      
      // For 1.4-compatibility rollover detection must be
      // handled explicitly by this MouseListener
      b.addMouseListener(rolloverListener);
      
      add((JComponent) b, index);

      return b;
    }
 
    /** Puts a horizontal bar into the menu.
     */ 
    public void addSeparator(int pos)
    {
      add(new JSeparator(), pos);
    }

    /** Creates a menu with the title <code>s</code> and the possibility
     * to expand and collapse the menu entries.
     * 
     * @param s
     */
    public CollapsePaneMenu(String s)
    {
      super(s);
      
      setBackground(SIDEMENU_MENU_BACKGROUND);
      setBorder(SIDEMENU_MENU_BORDER);

    }
    
    /**
     * ButtonUI implementation that provides a rollover effect
     * and delegates to another ButtonUI instance to paint
     * the rest of the component.
     *
     */
    private static class RolloverUI extends BasicButtonUI
    {
      ButtonUI ui;
      
      Color old;
      
      Color dark;
      
      RolloverUI(ButtonUI ui)
      {
        this.ui = ui;
        
        dark = new Color(UIManager.getLookAndFeelDefaults().getColor("Button.darkShadow").getRGB());
      }
      
      public void update(Graphics g, JComponent c)
      {
        if (old == null)
          old = c.getBackground();

        ButtonModel m = ((AbstractButton) c).getModel();
        if (m.isRollover())
          {
            c.setBackground(dark);
          }
        else
          {
            if (c.getBackground() != old)
              c.setBackground(old);
          }
        
        ui.update(g, c);
      }
    }
    
    /**
     * <code>MouseListener</code> implementation that handles the
     * rollover <em>detection</em>.
     * 
     * <p>It is only needed for 1.4-compatibility.</p>
     *
     */
    private static class RolloverListener extends MouseAdapter
    {
      public void mouseExited(MouseEvent me)
      {
        ((AbstractButton) me.getSource()).getModel().setRollover(false);
      }
      
      public void mouseEntered(MouseEvent me)
      {
        ((AbstractButton) me.getSource()).getModel().setRollover(true);
      }
      
    }

    
  }
}