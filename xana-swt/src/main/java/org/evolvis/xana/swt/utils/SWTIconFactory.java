/**
 * 
 */
package org.evolvis.xana.swt.utils;

import java.io.IOException;
import java.net.URL;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Display;
import org.evolvis.xana.utils.AbstractIconFactory;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SWTIconFactory extends AbstractIconFactory {

	private static SWTIconFactory instance = new SWTIconFactory();

	private SWTIconFactory()
	{
		// Do not instantiate
	}

	/** Returns Icon Factory. */
	public static SWTIconFactory getInstance()
	{
		return instance;
	}

	/**
	 * @see org.evolvis.xana.utils.AbstractIconFactory#getImageObjectFromURL(java.net.URL)
	 */
	@Override
	protected Object getImageObjectFromURL(URL loadURL) throws IOException {
		return new Image(Display.getCurrent(), new ImageData(loadURL.openStream()));
	}
}
