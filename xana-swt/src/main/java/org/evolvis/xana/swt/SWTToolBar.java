package org.evolvis.xana.swt;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javax.swing.Action;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Decorations;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.evolvis.xana.action.ActionContainer;
import org.evolvis.xana.action.ActionContainerException;
import org.evolvis.xana.action.ActionRegistry;
import org.evolvis.xana.action.TarentGUIAction;
import org.evolvis.xana.action.ToolBarHelper;
import org.evolvis.xana.swt.utils.SWTActionContainerHelper;

/**
 * 
 */

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SWTToolBar implements ActionContainer {

	protected final static Logger logger = Logger.getLogger(SWTToolBar.class.getName());
	
	protected String uniqueName;
	protected ResourceBundle resourceBundle;
	protected Decorations parent;
	protected ToolBar toolBar;
	
	/**
	 * Whether to insert separators between action-groups 
	 */
	protected boolean insertSeparators = false;
	
	/**
	 * Contains the group-priorities of the attached actions
	 */
	protected List<Integer> itemGroupPriorities;
	/**
	 * Contains the action-priorities of the attached actions
	 */
	protected List<Integer> itemActionPriorities;

	
	public SWTToolBar(String uniqueName, ResourceBundle resourceBundle, Decorations parent) {
		toolBar = new ToolBar(parent, SWT.HORIZONTAL);
        this.uniqueName = uniqueName;
        this.resourceBundle = resourceBundle;
        this.parent = parent;
        this.itemGroupPriorities = new ArrayList<Integer>();
        this.itemActionPriorities = new ArrayList<Integer>();
	}
	
	public ToolBar getToolBar() {
		return toolBar;
	}
	
	/**
	 * @see org.evolvis.xana.action.ActionContainer#attachGUIAction(javax.swing.Action, java.lang.String)
	 */
	public void attachGUIAction(final Action action, String menuPath) throws ActionContainerException {
		
		ToolBarHelper.checkType(action);
		ToolBarHelper.checkPath(action, menuPath);
		
		Integer groupPriority = Integer.parseInt(action.getValue(ToolBarHelper.GROUP_PRIORITY).toString());
		Integer actionPriority = Integer.parseInt(action.getValue(ToolBarHelper.ACTION_PRIORITY).toString());
		
		// Poss. add separators
		if(
				insertSeparators &&
				
				// First group should not have a separator
				groupPriority != 1 &&
				
				// If we found a new group, add a separator
				!itemGroupPriorities.contains(groupPriority)) {
			
			System.out.println("found new group!");

			int indexToInsert = getInsertPosition(groupPriority, 0);
			
			ToolItem item = new ToolItem(toolBar, SWT.SEPARATOR, indexToInsert);
			item.setWidth(10);
			
			itemGroupPriorities.add(indexToInsert, groupPriority);
			itemActionPriorities.add(indexToInsert, actionPriority);
		}
		
		
		int indexToInsert = getInsertPosition(groupPriority, actionPriority);
		
		final ToolItem item = new ToolItem(toolBar, SWT.PUSH, indexToInsert);
		itemGroupPriorities.add(indexToInsert, groupPriority);
		itemActionPriorities.add(indexToInsert, actionPriority);
		item.setText(action.getValue(Action.NAME).toString());
		item.setImage((Image)action.getValue(Action.SMALL_ICON));
		item.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				SWTToolBar.this.getToolBar().getDisplay().asyncExec(new Runnable() {
					
					public void run() {
						action.actionPerformed(new ActionEvent(SWTToolBar.this.getToolBar(), 0, ""));
					}
				});
			}
		});
		
		
		// Update enabled-state of ToolItem if enabled-state of action changed 
		SWTActionContainerHelper.addEnabledListener(item, action);
	}

	
	protected int getInsertPosition(Integer groupPriority, Integer actionPriority) {
		int indexToInsert = 0;

		// Compare the values of the action to be inserted with the already inserted actions 
		for(; indexToInsert < itemGroupPriorities.size(); indexToInsert++) {
			
			Integer currentGroupPriority = itemGroupPriorities.get(indexToInsert);
			Integer currentActionPriority = itemActionPriorities.get(indexToInsert);

			if(groupPriority <= currentGroupPriority && actionPriority <= currentActionPriority)
				// Found the insert-position for this element, return!
				return indexToInsert;
		}
		
		return indexToInsert;
	}
	
	/**
	 * @see org.evolvis.xana.action.ActionContainer#getContainerUniqueName()
	 */
	public String getContainerUniqueName() {
		return uniqueName;
	}

	/**
	 * @see org.evolvis.xana.action.ActionContainer#initActions()
	 */
	public void initActions() throws ActionContainerException {
		Iterator<TarentGUIAction> actionsIt = ActionRegistry.getInstance().getActions(getContainerUniqueName()).iterator();

		while(actionsIt.hasNext()) {
			TarentGUIAction action = actionsIt.next();
			attachGUIAction(action, action.getMenuPath(getContainerUniqueName()));
		}
	}

	/**
	 * @see org.evolvis.xana.action.ActionContainer#removeGUIAction(javax.swing.Action)
	 */
	public void removeGUIAction(Action arg0) {
		logger.warning("Removal of actions is currently not implemented");
	}
}
