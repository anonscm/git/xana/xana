/**
 * 
 */
package org.evolvis.xana.utils;

/**
 * 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface IconFactory {

	/**
	 * Get Toolkit-dependend (ImageIcon from Swing, Image from SWT,...) instance 
	 * of Icon for the given icon-id
	 *
	 * @param iconID
	 * @return
	 */
	public Object getIcon(String iconID);
}
